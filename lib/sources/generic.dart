import 'package:crypto/crypto.dart';

abstract class JiveMediaSource {
    // basic attributes and constructor
    String name;
    JiveMediaSource(this.name);

    // retrieving overall data
    void getAlbums();
    void getTracks();
    void getArtists();
    void getPlaylists();

    // retrieving specific data
    //void getAlbumInfo();
    //void getTrackInfo();
    //void getArtistInfo();
    //void getPlaylistInfo();
}

abstract class JiveNetworkMediaSource extends JiveMediaSource {
    // additional attributes and constructor for networked sources
    Uri url;
    String authenticationType;  // can be "token", "token-salt" or "password"
    JiveNetworkMediaSource(super.name, this.url, this.authenticationType);

    // authentication
    authenticate(String username, {String password, Digest token, String salt});
}
