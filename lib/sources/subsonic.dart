import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:get_it/get_it.dart';
import 'package:jive/services/audio.dart';
import 'package:jive/sources/generic.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart' as crypto;
import 'package:logger/logger.dart';

class SubsonicSource implements JiveNetworkMediaSource {
    @override
    String name;
    @override
    Uri url;
    @override
    late String authenticationType;

    String apiVersion = "v1.16.1";

    SubsonicSource(this.name, this.url) {
        GetIt.I<JiveAudioService>().registerSource(this);
        authenticationType = "token-salt";
    }

    String genSalt(int length) {
        String buffer = "";
        final rng = Random.secure();
        for (var i = 0; i < length; i++) {
            int rand = rng.nextInt(10);
            if (0 <= rand && rand < 3) {
                buffer += rng.nextInt(9).toString();
            } else if (3 <= rand && rand < 6) {
                buffer += String.fromCharCode(65 + rng.nextInt(25));
            } else {
                buffer += String.fromCharCode(97 + rng.nextInt(25));
            }
        }
        return buffer;
    }

    crypto.Digest genToken(String password, String salt) {
        Uint8List bytes = utf8.encode(password + salt);
        crypto.Digest token = crypto.md5.convert(bytes);
        return token;
    }

    Uri generateUri(String resource, String username, crypto.Digest token, String salt, Map<String, String> parameters) {
        String parameterString = "";
        for (var param in parameters.keys) {
           parameterString += "&$param=${parameters[param]}"; 
        }
        return Uri.parse("${url.toString()}/rest/$resource.view?u=$username&t=$token&s=$salt&c=jive&f=json&v=$apiVersion$parameterString");
    }

    @override
    Future<bool> authenticate(String username, {String? password, crypto.Digest? token, String? salt}) async {
        if (password != null || token == null || salt == null) {
            GetIt.I<Logger>().e("Subsonic source expects token and salt.");
            return false;
        } 
        final response = await http.get(generateUri("ping", username, token, salt, {}));
        Map<String, dynamic> result = (jsonDecode(response.body) as Map<String, dynamic>)["subsonic-response"];
        return result["status"] != "failed";
    }

    @override
    void getAlbums() {

    }

    @override
    void getArtists() {

    }

    @override
    void getPlaylists() {

    }

    @override
    void getTracks() {

    }
}
