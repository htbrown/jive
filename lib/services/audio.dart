import "package:audio_service/audio_service.dart";
import "package:get_it/get_it.dart";
import "package:jive/sources/generic.dart";
import "package:just_audio/just_audio.dart";
import "package:logger/logger.dart";

class JiveAudioService extends BaseAudioHandler {
    final _player = AudioPlayer();
    List<JiveMediaSource> sources = [];

    JiveAudioService() {
        _player.setUrl("https://incompetech.com/music/royalty-free/mp3-royaltyfree/That%20Zen%20Moment.mp3"); 
    }

    void registerSource(JiveMediaSource source) async {
        sources.add(source);
        if (source is JiveNetworkMediaSource && !await source.authenticate("hayden", password: "password")) {
            GetIt.I<Logger>().w("Failed to authenticate while registering ${source.runtimeType} with name '${source.name}' to audio handler. It will still be registered."); 
        } else {
            GetIt.I<Logger>().i("Authentication succeeded while registering ${source.runtimeType} with name '${source.name}' to audio handler.");
        }
        GetIt.I<Logger>().i("New ${source.runtimeType} with name '${source.name}' registered to audio handler (source ID: ${sources.indexOf(source)}).");
    }

    @override
    Future<void> play() async {
        _player.play();
    }
    @override
    Future<void> pause() => _player.pause();
}
