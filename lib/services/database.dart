import 'dart:async';
import 'dart:io';

import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class JiveDatabaseService {
    late Future<Database> database;
    final int databaseVersion = 1;

    Future<void> open() async {
        try {
            database = openDatabase(
                join(await getDatabasesPath(), "jive.db"),
                onCreate: (db, version) {
                    return db.execute(
                        "CREATE TABLE test (id INTEGER PRIMARY KEY, description TEXT)"
                    );
                },
                version: databaseVersion
            );
        } catch (e) {
            GetIt.I<Logger>().f("Failed to open database. Aborting...", error: e);
            exit(1);
        }
        GetIt.I<Logger>().i("Database opened at path '${join(await getDatabasesPath(), "jive.db")}'.");
    }
}
