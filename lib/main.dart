import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jive/services/audio.dart';
import 'package:jive/services/database.dart';
import 'package:jive/services/source.dart';
import 'package:jive/sources/subsonic.dart';
import 'package:jive/ui/pages/home.dart';
import 'package:logger/logger.dart';


Future<void> main() async {
    GetIt.I.registerSingleton<JiveAudioService>(
        await AudioService.init(
            builder: () => JiveAudioService(),
            config: const AudioServiceConfig(
                androidNotificationChannelId: 'com.htbrown.jive.channel.audio',
                androidNotificationChannelName: 'Audio playback',
                androidNotificationOngoing: true,
            ),
        )
    );
    GetIt.I.registerSingleton<JiveDatabaseService>(JiveDatabaseService());
    GetIt.I.registerSingleton<JiveSourceService>(JiveSourceService());
    GetIt.I.registerSingleton<Logger>(Logger());

    GetIt.I<JiveDatabaseService>().open();

    SubsonicSource("Hayden's Subsonic", Uri.dataFromString("https://navi.htbrown.net"));

    runApp(const JiveApp());
}

class JiveApp extends StatelessWidget {
  const JiveApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: HomePage(), 
    );
  }
}
