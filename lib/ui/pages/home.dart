import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jive/services/audio.dart';

class HomePage extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(title: const Text("hello")),
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        ElevatedButton(onPressed: GetIt.I<JiveAudioService>().play, child: const Text("Play")),
                        ElevatedButton(onPressed: GetIt.I<JiveAudioService>().pause, child: const Text("Pause")),
                    ],
                )
            )
        );
    }
}
