# jive

**A cross-platform, library-focused music player built with Flutter.**

This is a very work-in-progress attempt at building a modular music player with Flutter. It's probably best not to rely on it until I've managed to get somewhere, if that ever happens.


## Goals

1. Ensure the music player is as modular as possible.
    - This currently means making it easy to add new media sources. Right now, I'm focusing on bringing Subsonic API support to it in a way which allows other sources to be easily added in future.
2. Focus on functionality over user-interface at an early stage.
    - Once I get a working music player that can connect with at least Subsonic/Navidrome, I'll begin looking properly at UI. The idea is to have something similar to what I started [here](https://gitlab.com/jiveapp/jivedesktop).
    - The aim is to have a reasonably functional music player which integrates with the operating system as one would expect.
3. Desktop only.
    - Just to keep things simple for now.
